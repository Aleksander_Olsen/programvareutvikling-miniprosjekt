import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class CellRenderer extends JLabel implements TableCellRenderer {

	String type;
	String temp;
	Components comp = null;

	public CellRenderer(String _type) {
		setOpaque(true);
		setHorizontalAlignment(CENTER);
		setVerticalAlignment(CENTER);
		comp = new Components(0);

		type = _type;

	}

	// This is the method that is called to get the label to be drawn in the cell
	// Contains reference to the actual table, the value to be drawn,
	// whether the element is selected and focused and the row and column of the
	// element
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		if (isSelected) {
			setBackground(table.getSelectionBackground());
		} else {
			setBackground(table.getBackground());
		}

		if (type == "anchor") {
			setText("");
			setIcon(new ImageIcon(getClass().getResource(
					"Icons/anchor_" + value + ".png")));
		} else if (type == "fill") {

			//Switch to get the corresponding filename for the fill icons
			switch (value.toString()) {
			case "NONE":
				temp = "ingen";
				break;
			case "HORIZONTAL":
				temp = "horisontalt";
				break;
			case "VERTICAL":
				temp = "vertikalt";
				break;
			case "BOTH":
				temp = "begge";
				break;
			}

			setText("");
			setIcon(new ImageIcon(getClass().getResource(
					"Icons/skaler_" + temp + ".png")));

		}
		return this;
	}
}
