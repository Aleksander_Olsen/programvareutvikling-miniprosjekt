import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

//This class creates the popup menu when you right click.
public class createPopupMenu implements ActionListener, ItemListener {

	JPopupMenu popupMenu;
	JMenuItem item;
	String type;
	int atRow;
	
	private GBLModel theModel = MainRender.model;

	public createPopupMenu(int _atRow) {
		//Create a PopupMenu
		popupMenu = new JPopupMenu("Popup");

		//Find which row was clicked
		type = (theModel.getValueAt(_atRow, 0)).toString();
		atRow = _atRow;

		item = new JMenuItem((String)Singleton.getMessages().getString("popupProp"));
		item.addActionListener(this);
		popupMenu.add(item);
		item = new JMenuItem((String)Singleton.getMessages().getString("popupDel"));
		item.addActionListener(this);
		popupMenu.add(item);

	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if (e.getStateChange() == ItemEvent.SELECTED) {
			//Auto generated stuff.
		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//Check what right click menu was clicked.
		JMenuItem source = (JMenuItem)(e.getSource());
		if(source.getText() == (String)Singleton.getMessages().getString("popupProp")){
			displayPropertyWindow();
		} else if (source.getText() == (String)Singleton.getMessages().getString("popupDel")) {
			System.out.println("delete");
			theModel.deleteRow(atRow);
		}
	}

	public void displayPopUp(MouseEvent e) {
		//Display the popup menu.
		popupMenu.show(e.getComponent(), e.getX(), e.getY());

	}

	public void displayPropertyWindow() {
		//Display the property window.
		PropertyManager propertyManager = new PropertyManager(type, atRow); 
		propertyManager.setupPropertyWindow();

	}
}
