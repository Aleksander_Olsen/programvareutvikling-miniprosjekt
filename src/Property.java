
//Properties for the types that has special properties;
//JTextField, JTextArea, JList etc.
public class Property {
	private int rows;
	private int columns;
	private int width;
	private int height;

	private String JSpinnerListValue;

	private Boolean scroll;
	private Boolean wraping;
	private Boolean defaultComboBox;

	public Property() {
		//Set all the properties to the start values.
		rows = 0;
		columns = 0;
		width = 0;
		height = 0;

		//The string from the JSpinnerList type 
		//that will separate each element by ","
		JSpinnerListValue = "";

		scroll = true;
		wraping = true;
		defaultComboBox = true;
	}

/*---------------GETTERS AND SETTERS BELOW---------------*/
	/**
	 * @return the rows
	 */
	public int getRows() {
		return rows;
	}

	/**
	 * @param rows
	 *            the rows to set
	 */
	public void setRows(int rows) {
		this.rows = rows;
	}

	/**
	 * @return the columns
	 */
	public int getColumns() {
		return columns;
	}

	/**
	 * @param columns
	 *            the columns to set
	 */
	public void setColumns(int columns) {
		this.columns = columns;
	}

	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @param width
	 *            the width to set
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @param height
	 *            the height to set
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * @return the scroll
	 */
	public Boolean getScroll() {
		return scroll;
	}

	/**
	 * @param scroll
	 *            the scroll to set
	 */
	public void setScroll(Boolean scroll) {
		this.scroll = scroll;
	}

	/**
	 * @return the wraping
	 */
	public Boolean getWraping() {
		return wraping;
	}

	/**
	 * @param wraping
	 *            the wraping to set
	 */
	public void setWraping(Boolean wraping) {
		this.wraping = wraping;
	}

	/**
	 * @return the defaultComboBox
	 */
	public Boolean getDefaultComboBox() {
		return defaultComboBox;
	}

	/**
	 * @param defaultComboBox
	 *            the defaultComboBox to set
	 */
	public void setDefaultComboBox(Boolean defaultComboBox) {
		this.defaultComboBox = defaultComboBox;
	}

	/**
	 * @return the jSpinnerListValue
	 */
	public String getJSpinnerListValue() {
		return JSpinnerListValue;
	}

	/**
	 * @param jSpinnerListValue the jSpinnerListValue to set
	 */
	public void setJSpinnerListValue(String jSpinnerListValue) {
		JSpinnerListValue = jSpinnerListValue;
	}

}
