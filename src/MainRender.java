import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.NoSuchElementException;
import java.util.Scanner;

import javax.swing.AbstractAction;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;






@SuppressWarnings("serial")
public class MainRender extends JFrame {
	
	//Global Objects for class MainRender---------------------------------
	
  JTable componentsTabel = null;
  //A static model used in project to get all the data about layout table
  public static GBLModel model = new GBLModel ();
  //Unique number for variable name.
  int uniqueNumber = 1;
  //Used for save and load
  private Scanner input;
  //The file to save to/load from
  private File f;

  //----------------------------------------------------------------------
  
  
  //Constructor for MainRender Class
  
  public MainRender () {
    super ((String)Singleton.getMessages().getString("GBLEditor"));

    //One class for the menu and on with icons in the toolbar
    menuBar();
    toolbar();

    
    //----------------------------------------------------------------------

    componentsTabel = new JTable (model);

    /**
     *  Setting up comboboxes to be drawn. Anchor and Fill have their own custome cellrenderer.
     */
    
    //ComboBoxes inside the table [JTypes] ---------------------------------
    JComboBox JTypes = new JComboBox (Components.getJTypes()); // Components to be drawn in the combobox
    TableColumn typeColumn = componentsTabel.getColumnModel().getColumn(0); // To be rendered in the first column
    typeColumn.setCellEditor (new DefaultCellEditor(JTypes));
  

    //-------------------------------------------------------------
    DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
    typeColumn.setCellRenderer(renderer);
    
    
    //Add a mouseclick listener to check for right click. create a popup window when the user right click
    componentsTabel.addMouseListener(new MouseAdapter() {
       public void mouseClicked(MouseEvent e) {
          if (SwingUtilities.isRightMouseButton(e) && e.getClickCount() == 1) {  	
    	     //Get the row that was clicked in the table.
        	 JTable target = (JTable)e.getSource(); //Returns what cell the user has clicked in.
      	     int row = target.getSelectedRow();
    	    
      	     //Create the popup menu when right clicked.
    	     createPopupMenu popupMenu;
    	     popupMenu = new createPopupMenu(row); // sending what kind of JType in the user clicked cell and what row it was in
    	     popupMenu.displayPopUp(e); //Displays the popup.
          }
       }
    });

    
    //ComboBox inside the table [skalar/aligments]
    
    JComboBox skaler = new JComboBox (Components.getAlignments());
    skaler.setRenderer(new DropDownRenderer("fill")); //Using our dropdownrenderer with JType fill
    typeColumn = componentsTabel.getColumnModel().getColumn(7); // Returns the cell wich to be drawn in
    typeColumn.setCellEditor(new DefaultCellEditor(skaler)); 
    typeColumn.setCellRenderer(new CellRenderer("fill")); // Set our cell renderer with JType fill
    
  //-------------------------------------------------------------
    
    //ComboBox inside the table [anchor]
    
    JComboBox anchor = new JComboBox (Components.getAnchors());
    anchor.setRenderer(new DropDownRenderer("anchor")); //Using our dropdownrenderer with JType anchor
    typeColumn = componentsTabel.getColumnModel().getColumn(8); // Returns the cell wich to be drawn in
    typeColumn.setCellEditor(new DefaultCellEditor(anchor)); 
    typeColumn.setCellRenderer(new CellRenderer("anchor"));// Set our cell renderer with JType anchor
    
  //-------------------------------------------------------------
    

    
    /**
     * 
     *  Setting up the size and adds our cells to a scroll pane. Makes it to a fast size
     *  
     */
    
    componentsTabel.getColumnModel().getColumn(7).setPreferredWidth(100); 
    componentsTabel.getColumnModel().getColumn(7).setPreferredWidth(100);

    componentsTabel.getColumnModel().getColumn(8).setPreferredWidth(200);
    componentsTabel.setAutoResizeMode (JTable.AUTO_RESIZE_OFF);
    getContentPane().add(new JScrollPane(componentsTabel));
 
    //Pack, set visible and set default close operation.
    pack ();
    setVisible (true);
	setDefaultCloseOperation (EXIT_ON_CLOSE);
  }
  
//----------Menubar layout----------
  void menuBar() {
	// Add menu bars for 'File', 'Edit' and 'Help'
    JMenuBar bar = new JMenuBar();
    JMenu fileMenu = new JMenu((String)Singleton.getMessages().getString("menuFile"));
    fileMenu.setMnemonic('F');
    JMenu editMenu = new JMenu((String)Singleton.getMessages().getString("menuEdit"));
    editMenu.setMnemonic('E');
    JMenu helpMenu = new JMenu((String)Singleton.getMessages().getString("menuHelp"));
    helpMenu.setMnemonic('H');
    
    //Add sub menus to menu bar
    //File menu:
    //Menu item New, where you start over.
    JMenuItem newBar = new JMenuItem((String)Singleton.getMessages().getString("menuNew"));
    newBar.setMnemonic('N');
    newBar.setIcon(new ImageIcon(getClass().getResource("Icons/NEW.gif")));
    newBar.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			model.deleteAllRow(); //Deleting all cells
			uniqueNumber = 1;
		}
	});
    fileMenu.add(newBar);
    
    //Menu item Load, where you load an existing layout.
    JMenuItem load = new JMenuItem((String)Singleton.getMessages().getString("menuLoad"));
    load.setMnemonic('L');
    load.setIcon(new ImageIcon(getClass().getResource("Icons/OPENDOC.gif")));
    load.addActionListener(new ActionListener(){
    	
    /**
     * Loading from file [user dialog prompts the user with option for where to load file
     */
	@Override
    public void actionPerformed(ActionEvent e) {
    	
		JFileChooser chooser = new JFileChooser(new File("."));
		chooser.setFileSelectionMode (JFileChooser.FILES_ONLY);
		if (chooser.showOpenDialog(MainRender.this)==JFileChooser.CANCEL_OPTION)
			return;
		f = chooser.getSelectedFile();

		try {
			input = new Scanner (f);
		} catch (FileNotFoundException fnfe) {
			System.err.println ((String)Singleton.getMessages().getString("fileNotFound")
								+ f.getAbsolutePath());
		}
		
		model.deleteAllRow();
		uniqueNumber = 1;
		Components comp = new Components(uniqueNumber++);
		try{
			while (input.hasNext()){
				
				/*
				 *  reading in all the types from file with space inbetween. Problems if the its space in a variable
				 */
				comp.setComponentsType(input.next());
				comp.setVariableName(input.next());
				comp.setText(input.next());
				comp.setRow(input.nextInt());
				comp.setColumn(input.nextInt());
				comp.setRows(input.nextInt());
				comp.setColumns(input.nextInt());
				comp.setAnchor(input.next());
				model.addRow(comp);
			}
		} catch (NoSuchElementException nsee) {
			System.err.println ((String)Singleton.getMessages().getString("fileFormat"));
			input.close();
			nsee.printStackTrace();
			System.exit(1);
        }catch (IllegalStateException ise) {
        	System.err.println ((String)Singleton.getMessages().getString("fileRead"));
        	input.close();
        	System.exit(1);
	    }
	}
    
	});
    fileMenu.add(load);
    
    //Menu item Save, where you save the current layout.
    JMenuItem save = new JMenuItem((String)Singleton.getMessages().getString("menuSave"));
    save.setMnemonic('S');
    save.setIcon(new ImageIcon(getClass().getResource("Icons/SAVE.gif")));
    save.addActionListener(new ActionListener(){
		
    	@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser chooser = new JFileChooser(new File("."));
			chooser.setFileSelectionMode (JFileChooser.FILES_ONLY);
			if (chooser.showSaveDialog(MainRender.this)==JFileChooser.CANCEL_OPTION)
				return;
			f = chooser.getSelectedFile();
			if (f.exists())
				if (JOptionPane.showConfirmDialog(MainRender.this, (String)Singleton.getMessages().getString("fileExists"), 
						(String)Singleton.getMessages().getString("fileConfirm"), JOptionPane.YES_NO_OPTION)!=JOptionPane.YES_OPTION)
					return;
			try (ObjectOutputStream oos = new ObjectOutputStream (new FileOutputStream(f))){
				model.save (oos);
			} catch (IOException ioe) {
				System.err.println ((String)Singleton.getMessages().getString("fileWrong"));
			}
		}
	});
    fileMenu.add(save);
    
    //Menu Item Save As, where you save the current layout with a new name.
    JMenuItem saveAs = new JMenuItem((String)Singleton.getMessages().getString("menuSaveAs"));
    saveAs.setMnemonic('A');
    saveAs.setIcon(new ImageIcon(getClass().getResource("Icons/SAVE.gif")));
    saveAs.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			JFileChooser chooser = new JFileChooser(new File("."));
			chooser.setFileSelectionMode (JFileChooser.FILES_ONLY);
			if (chooser.showSaveDialog(MainRender.this)==JFileChooser.CANCEL_OPTION)
				return;
			f = chooser.getSelectedFile();
			if (f.exists())
				if (JOptionPane.showConfirmDialog(MainRender.this, (String)Singleton.getMessages().getString("fileExists"),
						(String)Singleton.getMessages().getString("fileConfirm"), JOptionPane.YES_NO_OPTION)!=JOptionPane.YES_OPTION)
					return;
			try (BufferedWriter bw = new BufferedWriter (new FileWriter (f))){
				java.util.Vector<Components> comp =model.getData(); 
				for (int i=0; i<comp.size(); i++) {
					bw.write(comp.get(i).writeToFile());
					bw.newLine();
				}
			} catch (IOException ioe) {
				System.err.println ((String)Singleton.getMessages().getString("fileWrong"));
			}
		}
	});
    fileMenu.add(saveAs);
    
    //Add a separator.
    fileMenu.addSeparator ();
    
    //Menu item Preview, where you can generate a preview
    //Currently not used.
    JMenuItem preview = new JMenuItem((String)Singleton.getMessages().getString("menuPreview"));
    preview.setMnemonic('P');
    fileMenu.add(preview);
    
    //Menu item Generate Java Code, where you generate the java code.
    JMenuItem generate = new JMenuItem((String)Singleton.getMessages().getString("menuGenerate"));
    generate.setMnemonic('J');
    generate.setIcon(new ImageIcon(getClass().getResource("Icons/SAVEJAVA.gif")));
    generate.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			if(f == null) {
				JOptionPane.showMessageDialog(getParent(), (String)Singleton.getMessages().getString("generateSave"));
			} else {
				Generator generate = new Generator(f);
				generate.writeToFile();
			}
		}
	});
    fileMenu.add(generate);
    
    //Add a separator.
    fileMenu.addSeparator ();
    
    //Menu item Exit, where you exit the program.
    JMenuItem exit = new JMenuItem((String)Singleton.getMessages().getString("menuExit"));
    exit.setMnemonic('E');
    exit.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			System.exit(0);
		}
	});
    fileMenu.add(exit);
    
    
    //Edit menu:
    //Menu item New Row, where you add a new row to layout table.
    JMenuItem newRow = new JMenuItem((String)Singleton.getMessages().getString("menuNewRow"));
    newRow.setMnemonic('N');
    newRow.setIcon(new ImageIcon(getClass().getResource("Icons/NEWROW.gif")));
    newRow.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			model.addRow(new Components(uniqueNumber++));
		}
	});
    editMenu.add(newRow);
    
    //Help menu
    //Menu item Help, where you get help.
    //Not yet implemented.
    JMenuItem help = new JMenuItem((String)Singleton.getMessages().getString("menuHelp"));
    help.setMnemonic('H');
    help.setIcon(new ImageIcon(getClass().getResource("Icons/HELP.gif")));
    helpMenu.add(help);
    
    //Add a separator.
    helpMenu.addSeparator ();
    
    //Menu item About, where you get info about program.
    //Not yet implemented.
    JMenuItem about = new JMenuItem((String)Singleton.getMessages().getString("menuAbout"));
    about.setMnemonic('A');
    helpMenu.add(about);
    
    bar.add(fileMenu);
    bar.add(editMenu);
    bar.add(helpMenu);
    setJMenuBar(bar);
  }
  
//----------Toolbar Layout----------
  void toolbar () {
	
	//Create new toolbar
	JToolBar toolbar = new JToolBar();
	
	//Toolbar item New File, where you create a new table file.
    AbstractAction newFile = new AbstractAction((String)Singleton.getMessages().getString("toolNew"),
			new ImageIcon(getClass().getResource("Icons/NEW.gif"))) {
		  
	  @Override
	  public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub	
		  model.deleteAllRow();
		  uniqueNumber = 1;
	  }
	};
	newFile.putValue(AbstractAction.MNEMONIC_KEY, (int)'N');
	
	//Toolbar item Open File, where you open a saved layout.
	AbstractAction open = new AbstractAction((String)Singleton.getMessages().getString("toolOpen"),
			new ImageIcon(getClass().getResource("Icons/OPENDOC.gif"))) {
		
	  @Override
	  public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub	
		  

				JFileChooser chooser = new JFileChooser(new File("."));
				chooser.setFileSelectionMode (JFileChooser.FILES_ONLY);
				if (chooser.showOpenDialog(MainRender.this)==JFileChooser.CANCEL_OPTION)
					return;
				f = chooser.getSelectedFile();
				
				
			

				System.out.println(f.getAbsolutePath());
				try {
					input = new Scanner (f);
				} catch (FileNotFoundException fnfe) {
					System.err.println ((String)Singleton.getMessages().getString("fileNotFound")
							           + f.getAbsolutePath());
				}
				
				model.deleteAllRow();
				uniqueNumber = 0;
				
				Components comp;
				//looper igjennom lagrer en ny rad for v�r i filen. en loop er en rad.
				try{
					while (input.hasNext()){
						comp = new Components(uniqueNumber++);
						comp.setComponentsType(input.next());
						comp.setVariableName(input.next());
						comp.setText(input.next());
						comp.setRow(input.nextInt());
						comp.setColumn(input.nextInt());
						comp.setRows(input.nextInt());
						comp.setColumns(input.nextInt());
						comp.setSkaler(input.next());
						comp.setAnchor(input.next());
						model.addRow(comp);
					}
				}
				catch (NoSuchElementException nsee) {
					System.err.println ((String)Singleton.getMessages().getString("fileFormat"));
					input.close();
					nsee.printStackTrace();
					System.exit(1);
		  }
				catch (IllegalStateException ise) {
				System.err.println ((String)Singleton.getMessages().getString("fileRead"));
				input.close();
				System.exit(1);
		}	
	  }
	};
	open.putValue(AbstractAction.MNEMONIC_KEY, (int)'O');
	
	//Toolbar item Save File, where you save the current layout.
	AbstractAction save = new AbstractAction((String)Singleton.getMessages().getString("toolSave"),
			new ImageIcon(getClass().getResource("Icons/SAVE.gif"))) {
		
	  @Override
	  public void actionPerformed(ActionEvent arg0) {
		JFileChooser chooser = new JFileChooser(new File("."));
		chooser.setFileSelectionMode (JFileChooser.FILES_ONLY);
		if (chooser.showSaveDialog(MainRender.this)==JFileChooser.CANCEL_OPTION)
			return;
		f = chooser.getSelectedFile();
		if (f.exists())
			if (JOptionPane.showConfirmDialog(MainRender.this, (String)Singleton.getMessages().getString("fileExists"),
					(String)Singleton.getMessages().getString("fileConfirm"), JOptionPane.YES_NO_OPTION)!=JOptionPane.YES_OPTION)
				return;
		try (BufferedWriter bw = new BufferedWriter (new FileWriter (f))){
			java.util.Vector<Components> comp =model.getData(); 
			for (int i=0; i<comp.size(); i++) {
				bw.write(comp.get(i).writeToFile());
				bw.newLine();
			}
		} catch (IOException ioe) {
			System.err.println ((String)Singleton.getMessages().getString("fileWrong"));
		}	
	  }
	};
	save.putValue(AbstractAction.MNEMONIC_KEY, (int)'S');
	
	//Toolbar item Preview, where you can see a preview of the layout.
	//Not yet implemented
	AbstractAction preview = new AbstractAction((String)Singleton.getMessages().getString("toolSave"),
			new ImageIcon(getClass().getResource("Icons/ExecuteProject.gif"))) {
		
	  @Override
	  public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub	
	  }
	};
	preview.putValue(AbstractAction.MNEMONIC_KEY, (int)'P');
	
	//Toolbar item Generate java code, where you generate the java code for the layout.
	AbstractAction generate = new AbstractAction((String)Singleton.getMessages().getString("toolGenerate"),
			new ImageIcon(getClass().getResource("Icons/SAVEJAVA.gif"))) {
		
	  @Override
	  public void actionPerformed(ActionEvent arg0) {
		if(f == null) {
			JOptionPane.showMessageDialog(getParent(), (String)Singleton.getMessages().getString("generateSave"));
		} else {
			Generator generate = new Generator(f);
			generate.writeToFile();
		}
		
	  }
	};
	generate.putValue(AbstractAction.MNEMONIC_KEY, (int)'J');
	
	//Toolbar item Add Row, where you add a new row to the layout.
	AbstractAction addRow = new AbstractAction((String)Singleton.getMessages().getString("toolAdd"),
			new ImageIcon(getClass().getResource("Icons/NEWROW.gif"))) {
		
	  @Override
	  public void actionPerformed(ActionEvent arg0) {
		model.addRow(new Components(uniqueNumber++));	
	  }
	};
	addRow.putValue(AbstractAction.MNEMONIC_KEY, (int)'A');
	
	//Toolbar item Move Up, move the selected row one up
	AbstractAction moveUp = new AbstractAction((String)Singleton.getMessages().getString("toolMoveUp"),
			new ImageIcon(getClass().getResource("Icons/MoveRowUp.gif"))) {
		
	  @Override
	  public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub	
		
		  
		  
  	      int row = componentsTabel.getSelectedRow();
  	      model.moveRowUp(row);
		  
	  }
	};
	moveUp.putValue(AbstractAction.MNEMONIC_KEY, (int)'U');
	
	//Toolbar item Move Down, move the selected row one down.
	AbstractAction moveDown = new AbstractAction((String)Singleton.getMessages().getString("toolMoveDown"),
			new ImageIcon(getClass().getResource("Icons/MoveRowDown.gif"))) {
		
	  @Override
	  public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub	
		  
  	      int row = componentsTabel.getSelectedRow();
  	      model.moveRowDown(row);
		  
		  
	  }
	};
	moveDown.putValue(AbstractAction.MNEMONIC_KEY, (int)'N');
	
	//Toolbar item Help, where you can get help.
	//Not yet implemented.
	AbstractAction help = new AbstractAction((String)Singleton.getMessages().getString("toolHelp"),
			new ImageIcon(getClass().getResource("Icons/HELP.gif"))) {
		
	  @Override
	  public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub	
	  }
	};
	help.putValue(AbstractAction.MNEMONIC_KEY, (int)'N');
	
	//Add all the the toolbar items to the toolbar
	toolbar.add (newFile);
	toolbar.add (open);
	toolbar.add (save);
	toolbar.addSeparator();
	toolbar.add (preview);
	toolbar.add (generate);
	toolbar.addSeparator();
	toolbar.add (addRow);
	toolbar.add (moveUp);
	toolbar.add (moveDown);
	toolbar.addSeparator();
	toolbar.add (help);
	add (toolbar, BorderLayout.NORTH);
  }
  
/*---------------HERE STARTS THE MAIN FUNCTION---------------*/
  public static void main (String args[]) {
      //Create a new singleton object for use in project.
	  Singleton singelton = Singleton.getInstance( );
      //Create the main class that will contain everything.
	  MainRender MR = new MainRender ();
  }

}
