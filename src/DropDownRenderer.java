
import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/*
 * This class is used to make a custom render where we override functions
 * from the standard renderer. Primarily to set icons for alignments and anchor.
 * 
 * It get a string as parameter which decide what type it will set the icon.
 * 
 * */

public class DropDownRenderer extends JLabel implements ListCellRenderer {

	//Variables for type (alignment and anchor)
	Components comp = null;
	String type;
	String temp; 			

	public DropDownRenderer(String _type) {
		//Set the drop down visible.
		setOpaque(true);
		//Center it.
		setHorizontalAlignment(CENTER);
		setVerticalAlignment(CENTER);
		comp = new Components(0);

		//Set what type to match with.
		type = _type;
	}

	@Override
	public Component getListCellRendererComponent(JList list, Object value,
			int index, boolean isSelected, boolean cellHasFocus) {
		//Check if the dropdown menu is selected.
		if (isSelected) {
			setBackground(list.getSelectionBackground());
			setForeground(list.getSelectionForeground());
		} else {
			setBackground(list.getBackground());
			setForeground(list.getForeground());
		}

		//Check if it's anchor or alignment dropdown.
		if (type == "anchor") {
			//Get icon from file
			setIcon(new ImageIcon(getClass().getResource(
					"Icons/anchor_" + value.toString() + ".png")));
		} else if (type == "fill") {

			//Get type of alignment to find file name for icon
			switch (value.toString()) {
			case "NONE":
				temp = "ingen";
				break;
			case "HORIZONTAL":
				temp = "horisontalt";
				break;
			case "VERTICAL":
				temp = "vertikalt";
				break;
			case "BOTH":
				temp = "begge";
				break;
			}

			//Set right icon
			setIcon(new ImageIcon(getClass().getResource(
					"Icons/skaler_" + temp + ".png")));

		}
		return this;
	}
}
