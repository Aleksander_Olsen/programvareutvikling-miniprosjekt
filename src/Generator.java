import java.awt.Container;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.lang.*;

//This class is used to generate the java code for the layout
public class Generator {
	private GBLModel theModel;
	private PrintWriter toFile;
	private Components comp;
	private Property prop;
	
	private String type;
	private String variableName;
	private String text;
	private String line;
	private String gbl = "layout";
	
	private int row;
	private int column;
	private int rows;
	private int columns;
	
	private File f;
	private String filePath;
	private String name;
	private String generated;
	
	
	public Generator(File _f) {
		//Get the model when created
		theModel = MainRender.model;
		//Set the file used for saving and loading
		f = _f;
		//Get the file path.
		filePath = f.getAbsolutePath().toString();
		//Get the name for use in class name
		name = f.getName();
		//Remove the .java from the name.
		name = name.replaceAll(".java", "");
		//Add "Generated" to the name and file path so to not
		//rewrite the layout template
		generated = name + "Generated";
		filePath = filePath.replaceAll(name, generated);
		
	}
	
	public void writeToFile() {
		//Create a new PrintWriter object with the file name.
		try {
			//Standard layout for the generated java code.
			toFile = new PrintWriter(filePath, "UTF-8");
			toFile.println("import javax.swing.*;");
			toFile.println("import java.awt.*;");
			toFile.println("");
			toFile.println("");
			toFile.println("public class " + generated + " extends JPanel {");
			getStartValues();
			toFile.println("");
			toFile.println("  public " + generated + "(Container panel) {");
			toFile.println("    GridBagLayout " + gbl + " = new GridBagLayout();");
			toFile.println("    GridBagConstraints gbc = new GridBagConstraints();");
			toFile.println("    panel.setLayout (" + gbl + ");");
			getEndValues();
			toFile.println("    ");
			toFile.println("  }");
			toFile.println("");
			toFile.println("  public static void main(String[] args) {");
			toFile.println("    JFrame main = new JFrame();");
			toFile.println("    main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);");
			toFile.println("    " + generated + " " + name + " = new " + generated 
								+ "(main.getContentPane());");
			toFile.println("    main.pack();");
			toFile.println("    main.setVisible(true);");
			toFile.println("  }");
			toFile.println("}");
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {toFile.close();} catch (Exception ex) {}
		}
	}
	
	//Layout for the declarations.
	public void getStartValues() {
		int rowCount = theModel.getRowCount();
		
		for(int i = 0; i < rowCount; i++ ) {
			//Get the data for each row.
			comp = (Components) theModel.getData().elementAt(i);
			prop = comp.getProp();
			type = comp.getJType();
			variableName = comp.getVariableName();
			variableName = variableName.replaceAll(" ", "_");
			text = comp.getText();
			if(text == null) {
				text = "";
			}
			
			//Switch for the different types that have a specific declarations.
			switch (type) {
			case "JTextField":
				jTextFieldStart();
				break;
			case "JTextArea":
				jTextAreaStart();
				break;
			case "JList":
				jListStart();
				break;
			case "JComboBox":
				jComboBoxStart();
				break;
			case "JSpinnerList":
				jSpinnerListStart();
				break;
			default:
				genericStart();
				break;
			}
			
		}
	}
	
	//Layout for constructor.
	public void getEndValues() {
		int rowCount = theModel.getRowCount();
		
		for(int i = 0; i < rowCount; i++ ) {
			//Get data for each row
			comp = (Components) theModel.getData().elementAt(i);
			prop = comp.getProp();
			type = comp.getJType();
			variableName = comp.getVariableName();
			variableName = variableName.replaceAll(" ", "_");
			row = comp.getRow();
			column = comp.getColumn();
			rows = comp.getRows();
			columns = comp.getColumns();
			
			//Generic layout for GridBagLayout constraints
			line = "    gbc" + ".gridy = " + row + ";";
			toFile.println(line);
			line = "    gbc" + ".gridx = " + column + ";";
			toFile.println(line);
			line = "    gbc" + ".gridheight = " + rows + ";";
			toFile.println(line);
			line = "    gbc" + ".gridwidth = " + columns + ";";
			toFile.println(line);
			line = "    gbc" + ".anchor = java.awt.GridBagConstraints."
					+ comp.getAnchor() + ";";
			toFile.println(line);
			line = "    gbc" + ".fill = java.awt.GridBagConstraints."
					+ comp.getSkaler() + ";";
			toFile.println(line);
			
			//Switch for the types with special constructor layout
			switch (type) {
			case "JTextArea":
				jTextAreaEnd();
				break;
			case "JList":
				jListEnd();
				break;
			default:
				genericEnd();
				break;
			}
		}
	}
	
	//Generic declaration layout.
	public void genericStart() {
		line = "  " + type + " " + variableName + " = new " 
		+ type + " (\"" + text + "\");";
		toFile.println(line);
	}
	
	//Generic constructor layout.
	public void genericEnd() {
		line = "    " + gbl + ".setConstraints (" + variableName 
				+ ", gbc);";
		toFile.println(line);
		//Check if the height in properties is larger than 0.
		if (prop.getHeight() != 0) {
			line = "    " + variableName + ".setPreferredSize ( new java.awt.Dimension ("
					+ prop.getWidth() + ", " + prop.getHeight() + "));";
			toFile.println(line);
		}
		line = "    panel.add (" + variableName + ");";
		toFile.println(line);
	}
	
	//Declaration for JTextField elements
	public void jTextFieldStart() {
		line = "  " + type + " " + variableName + " = new " 
				+ type + " (\"" + text + "\"";
		//Check if the rows in properties is larger than 0.
		if (prop.getRows() != 0) {
			line += ", " + prop.getRows();
		}
		line += ");";
		toFile.println(line);
	}
	
	//Declaration for JTextArea elements
	public void jTextAreaStart() {
		line = "  " + type + " " + variableName + " = new " 
				+ type + " (\"" + text + "\"";
		//Check if the rows in properties is larger than 0.
		if (prop.getRows() != 0) {
			line += ", " + prop.getRows()
					+ ", " + prop.getColumns();
		}
		line += ");";
		toFile.println(line);
	}
	
	//Constructor for JTextArea elements
	public void jTextAreaEnd() {
		//Check if the ScrollPane is checked
		if (prop.getScroll() == true) {
			String newVar = variableName + "ScrollPane";
			line = "    JScrollPane " + newVar + " = new JScrollPane ("
					+ variableName + ");";
			toFile.println(line);
			//Check if the height in properties is larger than 0.
			if (prop.getHeight() != 0) {
				line = "    " + newVar + ".setPreferredSize (new java.awt.Dimension ("
						+ prop.getWidth() + ", " + prop.getHeight() + "));";
				toFile.println(line);
			}
			line = "    " + gbl + ".setConstraints (" + newVar + ", gbc);";
			toFile.println(line);
			line = "    panel.add (" + newVar + ");";
			toFile.println(line);
		} else {
			//Check if the height in properties is larger than 0.
			if (prop.getHeight() != 0) {
				line = "    " + variableName + ".setPreferredSize (new java.awt.Dimension ("
						+ prop.getWidth() + ", " + prop.getHeight() + "));";
				toFile.println(line);
			}
			line = "    " + gbl + ".setConstraints (" + variableName + ", gbc);";
			toFile.println(line);
			line = "    panel.add (" + variableName + ");";
			toFile.println(line);
		}
		//Check if the Wrapping is checked.
		if (prop.getWraping() == true) {
			line = "    " + variableName + ".setLineWrap (true);";
			toFile.println(line);
			line = "    " + variableName + ".setWrapStyleWord (true);";
			toFile.println(line);
		}
	}
	
	//Declaration for JList elements
	public void jListStart() {
		line = "  String " + variableName + "Data = \""
				+ text + "\";";
		toFile.println(line);
		//Check if the DefaultComboBox is checked.
		if(prop.getDefaultComboBox() == true) {
			line = "  DefaultComboBoxModel " + variableName
					+ "Model = new DefaultComboBoxModel ("
					+ variableName + "Data.split (\"[\\\\p{Punct}\\\\s]+\"));";
			toFile.println(line);
			line = "  " + type + " " + variableName
					+ " = new " + type + "(" + variableName
					+ "Model);";
			toFile.println(line);
		} else {
			line = "  " + type + " " + variableName + " = new " + type + "(" 
					+ variableName + "Data.split (\"[\\\\p{Punct}]+\"));";
			toFile.println(line);
		}
	}
	
	//Constructor for JList elements.
	public void jListEnd() {
		//Check if the ScrollPane is checked
		if (prop.getScroll() == true) {
			String newVar = variableName + "ScrollPane";
			line = "    JScrollPane " + newVar + " = new JScrollPane ("
					+ variableName + ");";
			toFile.println(line);
			//Check if the height in properties is larger than 0.
			if (prop.getHeight() != 0) {
				line = "    " + newVar + ".setPreferredSize (new java.awt.Dimension ("
						+ prop.getWidth() + ", " + prop.getHeight() + "));";
				toFile.println(line);
			}
			line = "    " + gbl + ".setConstraints (" + newVar + ", gbc);";
			toFile.println(line);
			line = "    panel.add (" + newVar + ");";
			toFile.println(line);
		} else {
			//Check if the height in properties is larger than 0.
			if (prop.getHeight() != 0) {
				line = "    " + variableName + ".setPreferredSize (new java.awt.Dimension ("
						+ prop.getWidth() + ", " + prop.getHeight() + "));";
				toFile.println(line);
			}
			line = "    " + gbl + ".setConstraints (" + variableName + ", gbc);";
			toFile.println(line);
			line = "    panel.add (" + variableName + ");";
			toFile.println(line);
		}
	}
	
	//Declaration for JComboBox elements
	public void jComboBoxStart() {
		text = text.replaceAll(" ", "");
		line = "  String " + variableName + "Data = \"" + text + "\";";
		toFile.println(line);
		//Check if the Wrapping is checked.
		if(prop.getDefaultComboBox() == true) {
			line = "  DefaultComboBoxModel " + variableName
					+ "Model = new DefaultComboBoxModel ("
					+ variableName + "Data.split (\"[\\\\p{Punct}\\\\s]+\"));";
			toFile.println(line);
			line = "  " + type + " " + variableName
					+ " = new " + type + "(" + variableName
					+ "Model);";
			toFile.println(line);
		} else {
			line = "  " + type + " " + variableName + " = new " + type + "(" 
					+ variableName + "Data.split (\"[\\\\p{Punct}]+\"));";
			toFile.println(line);
		}
	}
	
	//Declaration for JSpinnerList elements
	public void jSpinnerListStart() {
		String listValue = prop.getJSpinnerListValue();
		listValue = listValue.replaceAll(" ", "");
		System.out.println(listValue);
		line = "  String " + variableName + "Data = \""
				+ listValue + "\";";
		toFile.println(line);
		
		line = "  SpinnerListModel " + variableName 
				+ "Model = new SpinnerListModel (" + variableName
				+ "Data.split (\"[\\\\p{Punct}\\\\s]+\"));";
		toFile.println(line);
		line = "  JSpinner " + variableName + " = new JSpinner "
				+ "(" + variableName + "Model);";
		toFile.println(line);
	}
}