import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

//This class generate the property window for the different JTypes.
public class PropertyManager implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	//Data from the layout table:
	private GBLModel theModel = MainRender.model;
	private Components comp;
	private Property prop;

	String type;
	int row;
	JFrame propertyWindow;

	//Temporary data holder.
	java.util.Vector<Components> tempData;

	// setupJTextField
	JSpinner columns;
	JSpinner rows;
	JSpinner width;
	JSpinner height;

	String JSpinnerProperty;

	GBLModel tmp;

	JButton Cancel;
	JButton Ok;

	JCheckBox ScrollPaneBox;
	JCheckBox ComboBox;

	public PropertyManager(String _type, int _row) {
		row = _row;
		type = _type;
		//Set up property window
		propertyWindow = new JFrame((String)Singleton.getMessages().getString("propWindow") + type);
		//Get the data from the row that was clicked.
		comp = (Components) theModel.getData().elementAt(row);
		prop = comp.getProp();
		Ok = new JButton("Ok");
		Cancel = new JButton("Cancel");

		Ok.addActionListener(new OkActionListener());
		Cancel.addActionListener(new CancelActionListener());

	}

	public void setupJTextField() {
		/*
		 * Layout for the property box for JTextField with
		 * number of rows, height and width.
		 * */
		JPanel p = new JPanel(new GridLayout(4, 2, 1, 1));

		//value, min, max, step
		SpinnerModel rowsModel = new SpinnerNumberModel(prop.getRows(), 0, 100,
				1);
		SpinnerModel widthModel = new SpinnerNumberModel(prop.getWidth(), 0,
				100, 1);
		SpinnerModel heightModel = new SpinnerNumberModel(prop.getHeight(), 0,
				100, 1);

		p.add(new JLabel((String)Singleton.getMessages().getString("propRows")));
		rows = new JSpinner(rowsModel);
		p.add(rows);
		p.add(new JLabel((String)Singleton.getMessages().getString("propWidth")));
		width = new JSpinner(widthModel);
		p.add(width);
		p.add(new JLabel((String)Singleton.getMessages().getString("propHeight")));
		height = new JSpinner(heightModel);
		p.add(height);

		p.add(Ok);
		p.add(Cancel);

		propertyWindow.add(p);

	}

	public void setupJTextArea() {
		/*
		 * Layout for the property box for JTextArea with
		 * checkbox for scrollpane and word wrapping,
		 * and number of columns, row, height and width.
		 * */
		JPanel p = new JPanel(new GridBagLayout());
		JPanel buttons = new JPanel(new GridLayout(1, 2));
		JPanel mainPanel = new JPanel(new GridBagLayout());

		ScrollPaneBox = new JCheckBox((String)Singleton.getMessages().getString("propScroll"));
		ScrollPaneBox.setSelected(prop.getScroll());
		ComboBox = new JCheckBox((String)Singleton.getMessages().getString("propWrap"));
		ComboBox.setSelected(prop.getWraping());

		GridBagConstraints gc = new GridBagConstraints();

		// value, min, max, step
		SpinnerModel columnsModel = new SpinnerNumberModel(prop.getColumns(),
				0, 100, 1);
		SpinnerModel rowsModel = new SpinnerNumberModel(prop.getRows(), 0, 100,
				1);
		SpinnerModel widthModel = new SpinnerNumberModel(prop.getWidth(), 0,
				100, 1);
		SpinnerModel heightModel = new SpinnerNumberModel(prop.getHeight(), 0,
				100, 1);

		gc.weightx = 0.5;
		gc.weighty = 0.5;

		gc.gridx = 0;
		gc.gridy = 0;

		p.add(new JLabel((String)Singleton.getMessages().getString("propColumns")), gc);

		gc.gridx = 1;

		columns = new JSpinner(columnsModel);
		p.add(columns, gc);

		gc.gridx = 0;
		gc.gridy = 1;

		p.add(new JLabel((String)Singleton.getMessages().getString("propRows")), gc);

		gc.gridx = 1;

		rows = new JSpinner(rowsModel);
		p.add(rows, gc);

		gc.gridx = 0;
		gc.gridy = 2;

		p.add(new JLabel((String)Singleton.getMessages().getString("propWidth")), gc);

		gc.gridx = 1;

		width = new JSpinner(widthModel);
		p.add(width, gc);

		gc.gridx = 0;
		gc.gridy = 3;

		p.add(new JLabel((String)Singleton.getMessages().getString("propHeight")), gc);

		gc.gridx = 1;

		height = new JSpinner(heightModel);
		p.add(height, gc);

		gc.gridx = 0;
		gc.gridy = 4;

		p.add(ScrollPaneBox, gc);

		gc.gridx = 0;
		gc.gridy = 5;
		p.add(ComboBox, gc);

		gc.gridx = 0;
		gc.gridy = 0;

		mainPanel.add(p, gc);

		buttons.add(Ok);
		buttons.add(Cancel);

		gc.gridy = 1;
		mainPanel.add(buttons, gc);

		propertyWindow.add(mainPanel);

	}

	public void setupJList() {

		/*
		 * Layout for the property box for JList with
		 * checkbox for scrollpane and default combobox,
		 * and height and width.
		 * */
		JPanel p = new JPanel(new GridBagLayout());
		JPanel buttons = new JPanel(new GridLayout(1, 2));
		JPanel mainPanel = new JPanel(new GridBagLayout());

		ScrollPaneBox = new JCheckBox((String)Singleton.getMessages().getString("propScroll"));
		ScrollPaneBox.setSelected(prop.getScroll());
		ComboBox = new JCheckBox((String)Singleton.getMessages().getString("propCombo"));
		ComboBox.setSelected(prop.getDefaultComboBox());

		GridBagConstraints gc = new GridBagConstraints();

		SpinnerModel widthModel = new SpinnerNumberModel(prop.getWidth(), 0, 100, 1);
		SpinnerModel heightModel = new SpinnerNumberModel(prop.getHeight(), 0, 100, 1);

		gc.weightx = 0.5;
		gc.weighty = 0.5;

		gc.gridx = 0;
		gc.gridy = 0;

		p.add(new JLabel((String)Singleton.getMessages().getString("propWidth")), gc);

		gc.gridx = 1;

		width = new JSpinner(widthModel);
		p.add(width, gc);

		gc.gridx = 0;
		gc.gridy = 1;

		p.add(new JLabel((String)Singleton.getMessages().getString("propHeight")), gc);

		gc.gridx = 1;

		height = new JSpinner(heightModel);
		p.add(height, gc);

		gc.gridx = 0;
		gc.gridy = 2;

		p.add(ScrollPaneBox, gc);

		gc.gridx = 0;
		gc.gridy = 3;
		p.add(ComboBox, gc);

		gc.gridx = 0;
		gc.gridy = 0;

		mainPanel.add(p, gc);

		buttons.add(Ok);
		buttons.add(Cancel);

		gc.gridy = 1;
		mainPanel.add(buttons, gc);

		propertyWindow.add(mainPanel);

	}

	public void setupJComboBox() {
		/*
		 * Layout for the property box for JComboBox with
		 * checkbox for default combobox,
		 * and height and width.
		 * */
		JPanel p = new JPanel(new GridBagLayout());
		JPanel buttons = new JPanel(new GridLayout(1, 2));
		JPanel mainPanel = new JPanel(new GridBagLayout());

		ComboBox = new JCheckBox((String)Singleton.getMessages().getString("propCombo"));
		ComboBox.setSelected(prop.getDefaultComboBox());

		GridBagConstraints gc = new GridBagConstraints();

		SpinnerModel widthModel = new SpinnerNumberModel(prop.getWidth(), 0, 100, 1);
		SpinnerModel heightModel = new SpinnerNumberModel(prop.getHeight(), 0, 100, 1);

		gc.weightx = 0.5;
		gc.weighty = 0.5;

		gc.gridx = 0;
		gc.gridy = 0;

		p.add(new JLabel((String)Singleton.getMessages().getString("propWidth")), gc);

		gc.gridx = 1;

		width = new JSpinner(widthModel);
		p.add(width, gc);

		gc.gridx = 0;
		gc.gridy = 1;

		p.add(new JLabel((String)Singleton.getMessages().getString("propHeight")), gc);

		gc.gridx = 1;

		height = new JSpinner(heightModel);
		p.add(height, gc);

		gc.gridx = 0;
		gc.gridy = 2;

		p.add(ComboBox, gc);

		gc.gridx = 0;
		gc.gridy = 0;

		mainPanel.add(p, gc);

		buttons.add(Ok);
		buttons.add(Cancel);

		gc.gridy = 1;
		mainPanel.add(buttons, gc);

		propertyWindow.add(mainPanel);
	}

	public void setupJSpinnerList() {
		/*
		 * Layout for the property box for JSpinnerList with
		 * a input field for the elements in the list, separated with ","
		 * */
		JSpinnerProperty = JOptionPane
				.showInputDialog((String)Singleton.getMessages().getString("propSpinner"),
						prop.getJSpinnerListValue());
		doActionForThisType();
	}

	public void setupPropertyWindow() {
		/*
		 * Switch to determine which property window to show.
		 * */
		switch (type) {

		case "JTextField":
			setupJTextField();
			showPropertyWindow();
			break;
		case "JTextArea":
			setupJTextArea();
			showPropertyWindow();
			break;
		case "JList":
			setupJList();
			showPropertyWindow();
			break;
		case "JComboBox":
			setupJComboBox();
			showPropertyWindow();
			break;
		case "JSpinnerList":
			setupJSpinnerList();
		default:
			break;
		}

	}

	public void showPropertyWindow() {
		/*
		 * Show the property window.
		 * */
		propertyWindow.setVisible(true);
		propertyWindow.setSize(200, 200);
		propertyWindow.pack();

	}

	class OkActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent ae) {

			doActionForThisType();
			propertyWindow.dispose();
		}

	}

	class CancelActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent ae) {

			propertyWindow.dispose();

		}

	}

	public void doActionForThisType() {
		/*
		 * When clicked OK in property window,
		 * set the new property values for this rows properties.
		 * */
		switch (type) {

		case "JTextField":
			prop.setRows(((Integer) rows.getValue()).intValue());
			prop.setWidth(((Integer) width.getValue()).intValue());
			prop.setHeight(((Integer) height.getValue()).intValue());
			break;
		case "JTextArea":
			prop.setColumns(((Integer) columns.getValue()).intValue());
			prop.setRows(((Integer) rows.getValue()).intValue());
			prop.setWidth(((Integer) width.getValue()).intValue());
			prop.setHeight(((Integer) height.getValue()).intValue());
			prop.setScroll(((Boolean)ScrollPaneBox.isSelected()).booleanValue());
			prop.setWraping(((Boolean)ComboBox.isSelected()).booleanValue());
			break;
		case "JList":
			prop.setWidth(((Integer) width.getValue()).intValue());
			prop.setHeight(((Integer) height.getValue()).intValue());
			prop.setScroll(((Boolean)ScrollPaneBox.isSelected()).booleanValue());
			prop.setDefaultComboBox(((Boolean)ComboBox.isSelected()).booleanValue());
			break;
		case "JComboBox":
			prop.setWidth(((Integer) width.getValue()).intValue());
			prop.setHeight(((Integer) height.getValue()).intValue());
			prop.setDefaultComboBox(((Boolean)ComboBox.isSelected()).booleanValue());
			break;
		case "JSpinnerList":
			prop.setJSpinnerListValue(JSpinnerProperty.toString());		
		default:
			break;
		}

	}
}
