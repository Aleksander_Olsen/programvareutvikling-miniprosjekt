
import java.io.IOException;
import java.io.ObjectOutputStream;
import javax.swing.table.AbstractTableModel;


@SuppressWarnings("serial")
public class GBLModel extends AbstractTableModel {

	//Names for the different columns in the table
	private String columnNames[] = { (String)Singleton.getMessages().getString("cellType"), 
									 (String)Singleton.getMessages().getString("cellVar"), 
									 (String)Singleton.getMessages().getString("cellText"),
									 (String)Singleton.getMessages().getString("cellRow"),
									 (String)Singleton.getMessages().getString("cellColumn"), 
									 (String)Singleton.getMessages().getString("cellRows"), 
									 (String)Singleton.getMessages().getString("cellColumns"), 
									 (String)Singleton.getMessages().getString("cellFill"), 
									 (String)Singleton.getMessages().getString("cellAnchor") };

	//A vector with the data for each row
	static java.util.Vector<Components> data = new java.util.Vector<Components>();

	public GBLModel() {
		//Empty constructor just because reasons.
	}

	public void addRow(Components _component) {
		//Add a new row to the layout table with default component values.
		getData().add(_component);
		fireTableRowsInserted(getData().size() - 1, getData().size() - 1);
	}
	
	public boolean isCellEditable(int row, int col) {
		// Note that the data/cell address is constant,
		// no matter where the cell appears onscreen.
		if (col < 9) {
			return true;
		} else {
			return false;
		}
	}

	public void deleteRow(int _atRow) {
		//Delete a single row in the layout table.
		data.remove(_atRow);
		fireTableRowsDeleted(_atRow, _atRow);
		
	}
	
	public void deleteAllRow(){
		//Delete all the rows in the table (start new file)
		data.clear();
		fireTableRowsDeleted(0, getData().size());
	}
	
	public void save (ObjectOutputStream oos) {
		//Write to file row by row
		try {
			for (int i=0; i<data.size(); i++){
			oos.writeObject(data.get(i));
			}
		} catch (IOException ioe) {
			System.err.println ((String)Singleton.getMessages().getString("fileWrong2"));
		}
	}

	public void moveRowUp(int row) {
		//Move selected row one row up.
		if(row <= getRowCount() && row > 0){
			
			Components tempComp = data.get(row - 1);
			data.set(row - 1, data.get(row));
			data.set(row, tempComp);
			fireTableStructureChanged();
		}
		
	}
	
	public void moveRowDown(int row) {
		//Move selected row one row down
		if(row < getRowCount() && row >= 0){
			
			Components tempComp = data.get(row + 1);
			data.set(row + 1, data.get(row));
			data.set(row, tempComp);
			fireTableStructureChanged();
		}
		
	}

/*----------GETTER AND SETTERS BELOW----------*/
	
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub

		return columnNames.length;
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}
	
	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub

		return getData().size();
	}
	
	@Override
	public Class<?> getColumnClass(int col) {
        switch (col) {
            case 3:
                return Integer.class;
            case 4:
                return Integer.class;
            case 5:
                return Integer.class;
            case 6:
                return Integer.class;
                
            default:
                return String.class;
        }
	}

	@Override
	public Object getValueAt(int row, int col) {

		switch (col) {
		case 0:
			return (((Components) getData().elementAt(row)).getComponentsType());
		case 1:
			return ((Components) getData().elementAt(row)).getVariableName();
		case 2:
			return ((Components) getData().elementAt(row)).getText();
		case 3:
			return ((Components) getData().elementAt(row)).getRow();
		case 4:
			return ((Components) getData().elementAt(row)).getColumn();
		case 5:
			return ((Components) getData().elementAt(row)).getRows();
		case 6:
			return ((Components) getData().elementAt(row)).getColumns();
		case 7:
			return ((Components) getData().elementAt(row)).getSkaler();
		case 8:
			return ((Components) getData().elementAt(row)).getAnchor();

		}
		return null;
	}

	@Override
	public void setValueAt(Object value, int row, int col) {
		// Fetch the element to be changed
		Components comp = (Components) getData().elementAt(row);

		switch (col) {

		case 0:
			comp.setComponentsType(((String) value));
			break;

		case 1:
			comp.setVariableName(((String) value));
			break;

		case 2:
			comp.setText(((String) value));
			break;
		case 3:
			comp.setRow(((Integer) value).intValue());
			break;
		case 4:
			comp.setColumn(((Integer) value).intValue());
			break;
		case 5:
			comp.setRows(((Integer) value).intValue());
			break;
		case 6:
			comp.setColumns(((Integer) value).intValue());
			break;
		case 7:
			comp.setSkaler(((String) value));
			break;
		case 8:
			comp.setAnchor(((String) value));
			break;

		}
		fireTableCellUpdated(row, col);
	}
	
	public java.util.Vector<Components> getData() {
		return data;
	}

	public void setData(java.util.Vector<Components> data) {
		GBLModel.data = data;
	}

}
