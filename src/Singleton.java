import java.util.Locale;
import java.util.ResourceBundle;

public class Singleton {

	  //We use a singleton for internationalization.
	  private static Singleton singleton = new Singleton( );
   	  private static ResourceBundle messages;
	  private static Locale currentLocale;
   /* A private Constructor prevents any other 
    * class from constructing this
    */
   private Singleton(){
	   
	  currentLocale = Locale.getDefault();
      messages = ResourceBundle.getBundle("i18n.language", currentLocale);
   }
   
   /* Static 'instance' method */
   public static Singleton getInstance( ) {
      return singleton;
   }

	/**
	 * @return the messages
	 */
	public static ResourceBundle getMessages() {
		return messages;
	}
	
	/**
	 * @param messages the messages to set
	 */
	public static void setMessages(ResourceBundle messages) {
		Singleton.messages = messages;
	}
   
   
}