import java.io.Serializable;

//Class for the attributes in a row.
public class Components implements Serializable {
	private static final long serialVersionUID = 1L;

	//The different types available
	public static final String JType[] = {
		"JLabel","JButton", "JTextField",
		"JTextArea", "JCheckBox", "JList",
		"JComboBox", "JSpinnerList"
	};

	//The different alignment options.
	public static final String alignments[] = {
		
		"NONE", "HORIZONTAL", "VERTICAL", "BOTH"
		
	};
	
	//The different anchors
	public static final String anchors[] = {
		
		"CENTER", "NORTH", "NORTHEAST", "EAST", "SOUTHEAST",
		"SOUTH", "SOUTHWEST", "WEST", "NORTHWEST"
		
	};
	
	//A new property class for each row.
	Property prop = new Property();

	/**
	 * Strings
	 */
	String variableName;
	String text;
	String componentsType;
	String skaler;
	String anchor;
	String variable = "new";
	
	/**
	 * Integers
	 */
	int row;
	int rows;
	int column;
	int columns;


	public Components(String componentsType,
			String variableName, String text, int row, int rows, int column,
			int columns, String skaler, String anchor) {
		
		super();
		
		this.componentsType = componentsType;
		this.variableName = variableName;
		this.text = text;
		this.row = row;
		this.rows = rows;
		this.column = column;
		this.columns = columns;
		this.skaler = skaler;
		this.anchor = anchor;
	}

	//Constructor used where number is for a unique variable name.
	public Components(int number){
		this.componentsType = JType[0];
		this.variableName = variable + number;

		this.row = 1;
		this.rows = 1;
		this.column = 1;
		this.columns = 1;
		this.skaler = "NONE";
		this.anchor = "CENTER";
	}
	
	//Function used for write to file.
	public String writeToFile(){
		StringBuilder sb = new StringBuilder ();
		sb.append(getJType());
		sb.append(" ");
		sb.append(getVariableName());
		sb.append(" ");
		sb.append(getText());
		sb.append(" ");
		sb.append(getRow());
		sb.append(" ");
		sb.append(getColumn());
		sb.append(" ");
		sb.append(getRows());
		sb.append(" ");
		sb.append(getColumns());
		sb.append(" ");
		sb.append(getSkaler());
		sb.append(" ");
		sb.append(getAnchor());
		return sb.toString();
	}
	
//----------GETTERS AND SETTERS BELOW HERE----------
	
	public static String[] getJTypes(){
		return JType;
	}

	public String getSkaler() {
		return skaler;
	}

	public static String[] getAnchors() {
		return anchors;
	}
	
	public String getComponentsType() {
		return componentsType;
	}

	public String getVariableName() {
		return variableName;
	}

	public String getText() {
		return text;
	}

	public int getColumn() {
		return column;
	}

	public int getRow() {
		return row;
	}

	public int getRows() {
		return rows;
	}

	public int getColumns() {
		return columns;
	}

	public String getAnchor() {
		return anchor;
	}
	
    public String getJType() {
	    return componentsType;
    }
    
    
	public static String[] getAlignments() {
		return alignments;
	}
	
    public void setComponentsType (String type) {
	    componentsType = type;
    }
  
  
	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setRow(int row1) {
		row = row1;
	}

	public void setRows(int row2) {
		this.rows = row2;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

	public void setSkaler(String skaler) {
		this.skaler = skaler;
	}

	public void setAnchor(String anchor) {
		this.anchor = anchor;
	}

	public Property getProp() {
		return prop;
	}

	public void setProp(Property prop) {
		this.prop = prop;
	}
}
