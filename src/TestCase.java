import static org.junit.Assert.*;

import org.junit.Test;


public class TestCase {

	@Test
	public void test() {
		MainRender render = new MainRender();
		assertEquals(1, render.uniqueNumber);
		
		GBLModel model = new GBLModel();
		assertNotNull(getClass());
		
		Components comp = new Components(1);
		assertEquals(1, comp.getColumn());
		assertEquals(1, comp.getRow());
		assertEquals(1, comp.getColumns());
		assertEquals(1, comp.getRows());
		
		assertNull(comp.getText());
		assertEquals("new1", comp.getVariableName());
		assertEquals("JLabel", comp.getJType());
		assertEquals("NONE", comp.getSkaler());
		assertEquals("CENTER", comp.getAnchor());
		
		Property prop = new Property();
		assertTrue(prop.getScroll());
		assertTrue(prop.getWraping());
		assertTrue(prop.getDefaultComboBox());
		assertEquals(0, prop.getRows());
		assertEquals(0, prop.getColumns());
		assertEquals(0, prop.getWidth());
		assertEquals(0, prop.getHeight());
		assertNotNull(prop.getJSpinnerListValue());
	}

}
